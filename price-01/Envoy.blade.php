@servers(['web' => 'deployer@3.0.102.250'])

@setup
    $repository = 'git@gitlab.com:alex170/price.git';
    $releases_dir = '/var/www/app/releases';
    $app_dir = '/var/www/app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}/price-01
    composer install --prefer-dist --no-scripts -q -o
    php artisan config:cache
    php artisan route:cache
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/price-01/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/price-01/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/price-01/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }}/price-01 {{ $app_dir }}/current
@endtask
