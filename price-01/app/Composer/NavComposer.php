<?php

namespace App\Composer;

use Illuminate\View\View;
use App\Services\CarService;

class NavComposer {
    public function __construct(CarService $carService) {
        $this->carService = $carService;
    }

    public function compose(View $view) {
        $view->with('latestCar', $this->carService->getLatestCar());
    }
}
