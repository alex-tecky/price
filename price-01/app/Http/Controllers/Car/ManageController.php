<?php

namespace App\Http\Controllers\Car;

use App\Services\CarService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ManageController extends Controller
{
    public function __construct(CarService $carService) {
        $this->carService = $carService;
    }
    //
    public function index() {
        return view('car.manage.index', [
            'cars' => $this->carService->getAllCars()
        ]);
    }

    public function honda() {
        return view('car.manage.honda', [
            'cars' => $this->carService->getAllCars()
        ]);
    }

    public function add(Request $request) {
        $this->carService->addCar(
            $request->input('name'),
            $request->input('color'),
            $request->input('price')
        );

        return redirect()->route('car.manage.index');
    }

    public function addHonda(Request $request) {
        $this->carService->addCar(
            'Honda',
            $request->input('color'),
            $request->input('price')
        );

        return redirect()->route('car.manage.index');
    }
}
