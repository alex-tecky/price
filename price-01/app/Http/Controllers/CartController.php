<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ICartService;
use App\Exceptions\OutOfStockException;

class CartController extends Controller
{
    //
    public function __construct(ICartService $cartService) {
        $this->cartService = $cartService;
    }

    public function addToCart(Request $request) {

        $product = $request->input('product');
        if (!empty($product)) {
            try {
                $this->cartService->addToCart($product);
                return ['result' => 'success'];
            } catch (OutOfStockException $e) {
                return ['result' => 'no_stock'];
            }
        } else {
            return ['result' => 'fail'];
        }
    }
}
