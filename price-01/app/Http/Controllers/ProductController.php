<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;

class ProductController extends Controller
{
    //
    public function __construct(ProductService $productService) {
        $this->productService = $productService;
    }

    public function list() {
        return view('product.list', [
            'products' => $this->productService->getAll()
        ]);
    }
}
