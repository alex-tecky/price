<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{
    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    //
    public function signUp(Request $request) {
        return view('signup-form', [
            'error' => $request->session()->get('error')
        ]);
    }

    public function loginForm() {
        return view('login-form');
    }

    public function login(Request $request) {
        $credentials = $request->only('username', 'password');

        if (Auth::guard('web')->attempt($credentials)) {
            return redirect('/');
        } else {
            return redirect('/login');
        }
    }

    public function facebookRedirect() {
        return Socialite::with('facebook')->redirect();
    }

    public function facebookCallback() {
        $fbUser = Socialite::driver('facebook')->user();

        $dbUser = $this->userService->getUserByFacebook($fbUser);

        if ($dbUser) {
            $this->userService->linkUserWithFacebookId($dbUser->id, $fbUser->id);
            Auth::guard('web')->login($dbUser);
        } else {
            $id = $this->userService->createUser($fbUser->email, '');
            $this->userService->linkUserWithFacebookId($id, $fbUser->id);
            Auth::guard('web')->loginById($id);
        }

        return redirect('/');
    }

    public function logout() {
        Auth::guard('web')->logout();
        return redirect('/login');
    }

    public function createUser() {

    }

    public function showProfileForm() {
        return view('profile-upload');
    }

    public function uploadProfile(Request $request) {
        $request->file('profile')->store('profile', 's3');

        return redirect()->route('profile-upload');
    }
}
