<?php

namespace App\Http\Middleware;

use Closure;

class SignUpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $inputs = $request->all();
        foreach ($inputs as $key => $value) {
            if (strpos($value, 'price') !== FALSE) {
                $request->session()->flash('error', 'Contains reserved words: price');

                return redirect('/signup');
            }
        }
        return $next($request);
    }
}
