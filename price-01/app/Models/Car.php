<?php

namespace App\Models;

class Car {
    public function __construct($car) {
        $this->name = $car->name;
        $this->color = $car->color;
        $this->original_price = $car->original_price;
    }

    public function formattedPrice() {
        return 'HKD' . $this->original_price;
    }
}
