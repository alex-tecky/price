<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use App\Repositories\ProductRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\IProductRepository;
use App\Repositories\CachedProductRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $siteCode = getenv('SITE_CODE');
        $namespace = "\\App\\Sites\\{$siteCode}\\Controllers";
        $sitePath = app_path(). '/Sites/' .$siteCode;

        if (!empty($siteCode)) {
            $this->loadViewsFrom($sitePath. '/Views', strtolower($siteCode));
            $this->mergeConfigFrom($sitePath. '/config.php', strtolower($siteCode));

            Route::namespace($namespace)
                ->middleware('web')
                ->group($sitePath . '/routes.php');
        }

        //
        View::composer('nav', 'App\\Composer\\NavComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(IProductRepository::class, function() {
            return new CachedProductRepository(app(ProductRepository::class));
        });
    }
}
