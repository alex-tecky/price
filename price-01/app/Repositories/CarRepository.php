<?php

namespace App\Repositories;

use App\Models\Car;
use Illuminate\Support\Facades\DB;

class CarRepository {
    public function __construct() {
        $this->table = DB::table('car');
    }

    public function getAll() {
        $cars = [];
        $carsQuery = $this->table->get();
        foreach ($carsQuery as $car) {
            array_push($cars, new Car($car));
        };
        return $cars;
    }

    public function getLatest() {
        return new Car($this->table->latest()->first());
    }

    public function add($name, $color, $price) {
        $this->table->insert([
            'name' => $name,
            'color' => $color,
            'original_price' => $price
        ]);
    }
}
