<?php

namespace App\Repositories;

use App\Product;

interface IProductRepository {
    public function addProduct(Product $product);

    public function getAll();

    public function removeAllExpired();
}
