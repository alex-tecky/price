<?php

namespace App\Services;

interface ICartService {

    /**
     * @throw OutOfStockException
     * @throw ExpiredProductException
     */
    public function addToCart($productId);

}
