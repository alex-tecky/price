<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Request;

class PhotoService {
    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function addPhoto($photoName) {
        $this->request->session()->push('photos', $photoName);
    }

    public function getPhotos() {
        if ($this->request->session()->get('photos')) {
            return $this->request->session()->get('photos');
        } else {
            throw new \Exception();
        }
    }
}
