<?php

namespace App\Sites\Car;

class Application extends \Illuminate\Foundation\Application {

    public function bootstrapPath($path = '')
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'bootstrap-car'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}
