<?php

namespace App\Sites\Car\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller {
    public function index() {
        return view('car::welcome', [
            'facebook_secret' => config('car.facebook_secret_2')
        ]);
    }
}
