<?php

namespace App\Sites\House;

class Application extends \Illuminate\Foundation\Application {

    public function bootstrapPath($path = '')
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'bootstrap-house'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}
