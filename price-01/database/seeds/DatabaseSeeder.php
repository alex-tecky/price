<?php

use App\Services\UserService;
use Illuminate\Database\Seeder;
use App\Repositories\UserRepository;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $userRepository = new UserRepository();
        $userService = new UserService($userRepository);
        $userService->createUser('alex', '123456');
        // $this->call(UsersTableSeeder::class);
    }
}
