<?php

namespace Price\Helpers;

use Illuminate\Routing\Router;

class Pager {
    public function __construct(Router $router) {
        $this->router = $router;
    }

    public function getCurrentPage() {
        $params = $this->router->current()->parameters();
        return $params['page'];
    }
}
