
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');

const elements = Array.from(document.querySelectorAll('.car > div'));
for (let element of elements) {
    element.addEventListener('click', function() {
        element.classList.add('zoom');
    });
}
