<form method="POST" action="/signup">
    @csrf

    {{ $error }}

    <p>
        <label>
            <span>UserName</span>
            <input type="text" name="username">
        </label>
    </p>
    <p>
        <label>
            <span>First Name</span>
            <input type="text" name="firstname">
        </label>
    </p>
    <p>
        <label>
            <span>Password</span>
            <input type="password" name="password">
        </label>
    </p>
    <input type="submit">
</form>
