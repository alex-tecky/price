<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Price\Facades\Pager;
use Price\Facades\Random;
use Price\Helpers\RandomNumberGenerator;

// Route::get('/', function () {
//     $pager = app('pager');

//     // dump($pager);

//     return view('welcome');
// });


// Route::get('/category/{page}', function ($page) {
//     return Pager::getCurrentPage();
// });

// Route::get('/random', function () {
//     dump(Random::getRandom());
//     dump(app(RandomNumberGenerator::class)->getRandom());
//     return 'see dump la';
// });

Route::get('/bookmark/show', 'BookmarkController@show');
Route::get('/bookmark/show/{order}', 'BookmarkController@show');

Route::prefix('{locale}')->middleware('locale')->group(function() {
    Route::get('/car/show', 'Car\\AdController@show');
    Route::get('/car/upload', 'Car\\UploadController@show')->name('car.upload-form');
    Route::post('/car/upload', 'Car\\UploadController@upload')->name('car.upload');

    Route::middleware('auth')->group(function() {
        Route::get('/car/manage/honda', 'Car\\ManageController@honda')->name('car.manage.honda');
        Route::post('/car/manage/honda', 'Car\\ManageController@addHonda')->name('car.manage.addHonda');
        Route::get('/car/manage', 'Car\\ManageController@index')->name('car.manage.index');
        Route::post('/car/manage', 'Car\\ManageController@add')->name('car.manage.add');
    });

    Route::get('/products', 'ProductController@list');
});

Route::resource('/photo', 'PhotoController')->middleware('secret:2');

Route::get('/signup', 'UserController@signUp');
Route::post('/signup', 'UserController@createUser')->middleware('signup');

Route::get('/login', 'UserController@loginForm');
Route::post('/login', 'UserController@login')->name('login');

Route::get('/login/facebook', 'UserController@facebookRedirect')->name('facebookLogin');
Route::get('/login/facebook/callback', 'UserController@facebookCallback');

Route::post('/logout', 'UserController@logout')->name('logout');

Route::get('/profile', 'UserController@showProfileForm')->name('profile-upload');
Route::post('/profile', 'UserController@uploadProfile');

