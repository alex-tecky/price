<?php

namespace Tests\Feature;

use Mockery;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\Services\CartService;
use App\Services\ICartService;
use App\Exceptions\OutOfStockException;
use App\Http\Controllers\CartController;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSuccessExample()
    {
        // Arrange
        $cartSerevice = Mockery::mock(CartService::class);
        $cartSerevice->shouldReceive('addToCart')
            ->with(1)
            ->andReturn(true);

        $request = Mockery::mock(Request::class, [
            'input' => 1
        ]);
        $cartController = new CartController($cartSerevice);

        // Act
        $array = $cartController->addToCart($request);

        // Assert
        $this->assertEquals(['result' => 'success'], $array);
    }

    public function testFailedWithoutInput()
    {
        // Arrange
        $cartSerevice = Mockery::mock(CartService::class);
        $cartSerevice->shouldNotReceive('addToCart');

        $request = Mockery::mock(Request::class, [
            'input' => ''
        ]);
        $cartController = new CartController($cartSerevice);

        // Act
        $array = $cartController->addToCart($request);

        // Assert
        $this->assertEquals(['result' => 'fail'], $array);
    }


    public function testFailedWithCartServiceReturnNoStock()
    {
        // Arrange
        $cartSerevice = Mockery::mock(ICartService::class);
        $cartSerevice->shouldReceive('addToCart')
            ->with(1)
            ->andThrow(OutOfStockException::class);

        $request = Mockery::mock(Request::class, [
            'input' => 1
        ]);
        $cartController = new CartController($cartSerevice);

        // Act
        $array = $cartController->addToCart($request);

        // Assert
        $this->assertEquals(['result' => 'no_stock'], $array);
    }
}
