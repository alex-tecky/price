<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use App\Repositories\ProductRepository;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductE2ETest extends TestCase
{
    use RefreshDatabase;

    public function testListProductsEn()
    {
        $response = $this->get('/en/products');

        $response->assertSeeText('No products available');
    }

    public function testListProductsZh()
    {
        $response = $this->get('/zh/products');

        $response->assertSeeText('沒有產品');
    }

    public function testListProductsWithProductEn()
    {
        $repository = new ProductRepository();
        $product = new Product([
            'date' => mktime(0, 0, 0, 1, 1, 2018),
            'price' => 1000,
            'expiry_date' => null,
            'bulk' => []
        ]);
        $repository->addProduct($product);

        $response = $this->get('/en/products');

        $response->assertDontSeeText('No products available');
    }

    public function testListProductsWithProductZh()
    {
        $repository = new ProductRepository();
        $product = new Product([
            'date' => mktime(0, 0, 0, 1, 1, 2018),
            'price' => 1000,
            'expiry_date' => null,
            'bulk' => []
        ]);
        $repository->addProduct($product);

        $response = $this->get('/zh/products');

        $response->assertDontSeeText('沒有產品');
    }
}
