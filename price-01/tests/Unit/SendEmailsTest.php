<?php

namespace Tests\Unit;

use Mockery;
use App\User;
use Tests\TestCase;
use App\Mail\GreetingMail;
use App\Console\Commands\SendEmails;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SendEmailsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSendEmailsCommand()
    {
        $user = new User((object) [
            'id' => 1,
            'username' => 'test@price.com.hk',
            'password' => '',
            'remember_token' => null
        ]);

        $userRepository = \Mockery::mock(UserRepository::class);
        $userRepository->shouldReceive('getAll')
            ->andReturn([
                $user
            ]);

        $send = \Mockery::mock();
        $send->shouldReceive('send')
            ->with(GreetingMail::class)
            ->andReturn(true);

        Mail::shouldReceive('to')
            ->with($user)
            ->andReturn($send);

        $command = new SendEmails($userRepository);
        $command->handle();
    }
}
